/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use anyhow::Error;
use futures::prelude::*;
use libinit::{debug, fmt_tree, initialize};
use std::time::Duration;
use tokio::spawn;

fn main() -> Result<(), Error> {
    // TODO: maybe a wrapper around main somehow?
    initialize(async {
        match debug::SerialDebugger::new() {
            Ok(d) => debug::set_global(d),
            Err(e) => {
                debug::set_global(debug::AsyncStdoutDebugger);
                debug!("could not setup serial logging: {:?}", e);
            }
        }

        // Start greeter running.
        spawn(
            tokio::time::interval(Duration::from_secs(5))
                .for_each(|_| async { println!("Hello!") }),
        );

        debug!("{}", fmt_tree("/").await.expect("could not print fs"));

        /*
        let events = input_events();
        pin_mut!(events);
        while let Some(event) = events.next().await {
            debug!("{:?}", event);
        }
        */

        tokio::time::delay_for(Duration::from_secs(60)).await;

        Ok(())
    })

    // TODO: exit cleanly or at least fork off into something that linux is willing to let exit()
}
