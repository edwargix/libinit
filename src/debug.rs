/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use anyhow::Error;
use futures::lock::Mutex;
use once_cell::sync::OnceCell;
use std::fmt;
use std::sync::Arc;
use tokio::{prelude::*, spawn};
use tokio_serial::Serial;

pub trait Debugger<T>: Sync + Send {
    fn send(&self, data: T);
}

pub struct NullDebugger;

impl<T> Debugger<T> for NullDebugger {
    fn send(&self, _: T) {}
}

pub struct StdoutDebugger;

impl<T: fmt::Display> Debugger<T> for StdoutDebugger {
    fn send(&self, data: T) {
        println!("{}", data);
    }
}

pub struct AsyncStdoutDebugger;

impl<T: fmt::Display> Debugger<T> for AsyncStdoutDebugger {
    fn send(&self, data: T) {
        use tokio::io::stdout;

        let text = format!("{}", data);
        spawn(async move {
            let _ = stdout().write_all(text.as_bytes()).await;
        });
    }
}

struct SerialDebuggerInner {
    write: Mutex<Serial>,
}

#[derive(Clone)]
pub struct SerialDebugger {
    inner: Arc<SerialDebuggerInner>,
}

impl SerialDebugger {
    pub fn new() -> Result<SerialDebugger, Error> {
        let file = Serial::from_path("/dev/ttyS0", &Default::default())?;

        Ok(SerialDebugger {
            inner: Arc::new(SerialDebuggerInner {
                write: Mutex::new(file),
            }),
        })
    }
}

impl<T: fmt::Display> Debugger<T> for SerialDebugger {
    fn send(&self, data: T) {
        let text = format!("{}", data);
        let inner = self.inner.clone();
        spawn(async move {
            let mut write = inner.write.lock().await;
            let _ = write.write_all(text.as_bytes()).await;
        });
    }
}

static GLOBAL_DEBUGGER: OnceCell<Box<dyn for<'a> Debugger<fmt::Arguments<'a>>>> = OnceCell::new();

pub fn debug(args: std::fmt::Arguments) {
    GLOBAL_DEBUGGER.get().expect("no debugger set").send(args);
}

pub fn set_global(dbg: impl for<'a> Debugger<fmt::Arguments<'a>> + 'static) {
    GLOBAL_DEBUGGER
        .set(Box::new(dbg))
        .map_err(|_| ())
        .expect("debugger already set");
}
