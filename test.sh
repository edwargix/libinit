#!/bin/sh

# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

set -xe

WORK_DIR=$(mktemp -d)
cargo build --target x86_64-unknown-linux-musl --release --examples
cp "target/x86_64-unknown-linux-musl/release/examples/${1:-demo}" "$WORK_DIR/init"
cd "$WORK_DIR"
strip -s init
echo "./init" | cpio --create -H newc | gzip > initramfs.img
du -h *
qemu-system-x86_64 -enable-kvm -kernel /boot/vmlinuz-linux -initrd initramfs.img -append "RUST_BACKTRACE=1" -serial stdio
